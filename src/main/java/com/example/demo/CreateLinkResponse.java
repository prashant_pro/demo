package com.example.demo;

import lombok.Value;

@Value
public class CreateLinkResponse {
  private String shortenedLink;
}
