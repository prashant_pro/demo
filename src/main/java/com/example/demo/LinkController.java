package com.example.demo;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class LinkController {

  @PostMapping("/link")
  Mono<CreateLinkResponse> create(@RequestBody CreateLinkRequest request) {
    return Mono.just(new CreateLinkResponse("http://localhost:8080"));
  }


}
