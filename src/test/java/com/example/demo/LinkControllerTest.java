package com.example.demo;

import javax.swing.text.html.HTMLEditorKit.LinkController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = LinkController.class)
public class LinkControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    void shortens_link() {
        webTestClient.post().uri("/link").contentType(MediaType.APPLICATION_JSON)
                .bodyValue("{\"link\": \"https://localhost:8080\"}").exchange().expectStatus()
                .is2xxSuccessful();
    }

}
